import React from "react";
import ReactDOM from "react-dom/client";
import "./index.css";
import Calculator from "./main/calculator";

const root = ReactDOM.createRoot(document.getElementById("root"));
root.render(
  <React.Fragment>
    <React.StrictMode>
      <h1>Apple Macbook</h1>
      <Calculator />
    </React.StrictMode>
  </React.Fragment>
);
