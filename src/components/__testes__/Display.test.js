import { render, screen } from "@testing-library/react";
import Display from "../Display";

test("Showing render button component", () => {
  render(<Display />);
  screen.getByTestId("display_test");
});
