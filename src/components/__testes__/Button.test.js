import { render, screen } from "@testing-library/react";
import Button from "../Button";

test("Showing render button component", () => {
  render(<Button />);
  screen.getByTestId("buttons_test");
});
